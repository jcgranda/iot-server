/*jslint node: true */
'use strict';


var util = require('util'),
    Thing = require('./thing');


function Terrace(databasePath) {
    Thing.call(this, databasePath);
}

util.inherits(Terrace, Thing);


// Añade un valor de luz al histórico y como último valor
Terrace.prototype.addLight = function (light) {

    this._addNewInfo('light', light);
};


// Establece el valor de la alarma de acceso
Terrace.prototype.setAccessAlarm = function (alarm) {
    
    this._setAlarm('alarm_access', alarm);    
};


module.exports = new Terrace('terrace');
