/*jslint node: true */
'use strict';


var util = require('util'),
    EventEmitter = require('events').EventEmitter;


function Thing(databasePath) {
    EventEmitter.call(this);
}

util.inherits(Thing, EventEmitter);


// Actualiza el valor de una alarma
Thing.prototype._setAlarm = function (alarm, value) {

    if (value) {
        this.emit(alarm, value);
    }
};

// Actualiza el valor de un sensor en el histórico y como último valor
Thing.prototype._addNewInfo = function (unit, value) {

    console.log('New data: ' + unit + '(' + value + ')');
};
    

module.exports = Thing;
