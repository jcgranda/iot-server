/*jslint node: true */
'use strict';


var util = require('util'),
    Thing = require('./thing');


function Kitchen(databasePath) {
    Thing.call(this, databasePath);

}

util.inherits(Kitchen, Thing);


// Añade un valor de temperatura al histórico y como último valor
Kitchen.prototype.addTemperature = function (temperature) {

    this._addNewInfo('temperature', temperature);
};


// Establece el valor de la alarma de inundación
Kitchen.prototype.setFloodingAlarm = function (alarm) {
    
    this._setAlarm('alarm_flood', alarm);    
};


module.exports = new Kitchen('kitchen');
