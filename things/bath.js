/*jslint node: true */
'use strict';


var util = require('util'),
    Thing = require('./thing');


function Bath(databasePath) {
    Thing.call(this, databasePath);
}

util.inherits(Bath, Thing);


// Añade un valor de temperatura al histórico y como último valor
Bath.prototype.addTemperature = function (temperature) {

    this._addNewInfo('temperature', temperature);
};


// Establece el valor de la alarma de inundación
Bath.prototype.setFloodingAlarm = function (alarm) {
    
    this._setAlarm('alarm_flood', alarm);    
};


module.exports = new Bath('bath');
