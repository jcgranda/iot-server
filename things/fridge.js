/*jslint node: true */
'use strict';


var util = require('util'),
    Thing = require('./thing');


function Fridge(databasePath) {
    Thing.call(this, databasePath);
}

util.inherits(Fridge, Thing);


// Añade un valor de temperatura al histórico y como último valor
Fridge.prototype.addTemperature = function (temperature) {

    this._addNewInfo('temperature', temperature);
};


// Establece el valor de la alarma de temperatura alta
Fridge.prototype.setTemperatureAlarm = function (alarm) {
    
    this._setAlarm('alarm_temp', alarm);    
};


module.exports = new Fridge('fridge');
