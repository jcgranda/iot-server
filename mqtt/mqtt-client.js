/*jslint node: true */
'use strict';

const ROOT_TOPIC = 'house1';
const MQTT_BROKER = '156.35.151.85';


module.exports.listen = function () {

    var mqtt = require('mqtt'),
        fridge = require('../things/fridge'),
        kitchen = require('../things/kitchen'),
        bath = require('../things/bath'),
        terrace = require('../things/terrace');

    var client = mqtt.connect('mqtt://' + MQTT_BROKER);

    client.on('connect', function () {

        console.log('MQTT client connected');

        // Temas a los que suscribirse        
        client.subscribe(ROOT_TOPIC + '/+/sensor/#');
        client.subscribe(ROOT_TOPIC + '/+/+/sensor/#');
    })


    client.on('message', function (topic, message) {

        // Depende del tema
        switch (topic) {
            case ROOT_TOPIC + '/kitchen/fridge/sensor/temperature':
                fridge.addTemperature(parseInt(message));
                break;

            case ROOT_TOPIC + '/kitchen/fridge/sensor/alarm_temp':
                fridge.setTemperatureAlarm(message == '1');
                break;

            case ROOT_TOPIC + '/kitchen/sensor/temperature':
                kitchen.addTemperature(parseInt(message));
                break;

            case ROOT_TOPIC + '/kitchen/sensor/alarm_flood':
                kitchen.setFloodingAlarm(message == '1');
                break;

            case ROOT_TOPIC + '/bath/sensor/temperature':
                bath.addTemperature(parseInt(message));
                break;

            case ROOT_TOPIC + '/bath/sensor/alarm_flood':
                bath.setFloodingAlarm(message == '1');
                break;

            case ROOT_TOPIC + '/terrace/sensor/light':
                terrace.addLight(parseInt(message));
                break;

            case ROOT_TOPIC + '/terrace/sensor/alarm_access':
                terrace.setAccessAlarm(message == '1');
                break;

            default:
                console.log('Received unknown MQTT message: ' + topic + ' (' + message + ')');
        }
    });
}
