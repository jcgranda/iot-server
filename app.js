/********************************************************
 *  Servidor IoT.
 *  Se comunica con el broker MQTT para recibir información
 *  de los sensonres y enviarlos a Firebase. También envía
 *  comandos de actuación.
 *
 *  Curso de Verano: IoT. Del sensor al móvil
 *  Universidad de Oviedo
 *
 * *****************************************************/
/*jslint node: true */
'use strict';

var fridge = require('./things/fridge'),
    kitchen = require('./things/kitchen'),
    bath = require('./things/bath'),
    terrace = require('./things/terrace');


// Inicialización de las "cosas"
fridge.on('alarm_temp', function (alarm) {
    if (alarm) {
        console.log("Fridge: temperature alarm!!!");
    }
});
kitchen.on('alarm_flood', function (alarm) {
    if (alarm) {
        console.log("Kitchen: flooding alarm!!!");
    }
});
bath.on('alarm_flood', function (alarm) {
    if (alarm) {
        console.log("Bath: flooding alarm!!!");
    }
});
terrace.on('alarm_access', function (alarm) {
    if (alarm) {
        console.log("Terrace: access alarm!!!");
    }
});


var mqttClient = require('./mqtt/mqtt-client');
mqttClient.listen();
